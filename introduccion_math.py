#Importar biblioteca matemática
import math

numero: int = 4
"""
#Elevar un número a la potencia (numéro, potencia)
print( math.pow(2, 2) )

#Raíz cuadrada
print( math.sqrt(4) )

#Seno
print( math.sin(20) )

#Seno a la inversa
math.asin(numero)

#sin^2(numero)
math.pow( math.sin(numero), 2 )

#Coseno
math.cos(numero)
"""
#Convertir a radianes
math.radians()

numero_radianes = math.radians(numero)

seno_lat_cuadrado = math.pow( math.sin(numero_radianes), 2 )
coseno_lat_1 = math.cos(numero_radianes)
coseno_lat_2 = math.cos(numero_radianes)
seno_lng_cuadrado = math.pow( math.cos(numero_radianes), 2 )

formula_1 = seno_lat_cuadrado + coseno_lat_1 * coseno_lat_2 * seno_lng_cuadrado
#Raiz cuadrada
raiz = math.sqrt( formula_1 )
radio: float = 6372.79
distancia = 2 * radio * math.asin(numero_radianes)

